﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo1
{
    class factory
    {
        private string name;
        private string location;
        private string phone;

        public void setname(string name)        {
            this.name = name;
        }

        public string getname() {
            return name;
        }

        public void setlocation(string location) {
            this.location = location;
        }

        public string getlocation() { 
            return location;
        }

        public void setphone(string phone) {
            this.phone = phone;
        }

        public string getphone() {
            return phone;
        }

    }
}
