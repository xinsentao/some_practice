﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Car:Garage
    {
        //名字
        public string CarName { get; set; }
        //颜色
        public string Color { get; set; }
        //轮子数
        public int CarWheel { get; set; }
        

        //行为：跑 （判断是否够四个轮子，若不够送去修车厂）
        public void Insert() 
        {
            if (CarWheel == 4)
            {
                Console.WriteLine("车子合格，可以起跑。");
            }
            else
            {
         
                Console.WriteLine("车子不合格，需送回修车厂。");

                Garage.fix();
                Console.WriteLine("请输入修车厂的名字：");
                Name = Console.ReadLine();
                Console.WriteLine("请输入修车厂的地址：");
                Site = Console.ReadLine();
                Console.WriteLine("请输入修车厂的电话：");
                Phone = int.Parse (Console.ReadLine());
                Console.WriteLine("返厂成功！");
            }
        }
    }
}
