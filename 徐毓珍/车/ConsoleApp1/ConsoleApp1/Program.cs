﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();

            Console.WriteLine("请输入车子的信息");
            Console.WriteLine("请输入车子的名字:");
            car.CarName = Console.ReadLine();

            Console.WriteLine("请输入车子的颜色:");
            car.Color = Console.ReadLine();

            Console.WriteLine("请输入车子的轮子数:");
            car.CarWheel = int.Parse(Console.ReadLine());

            //判断
            car.Insert();
        }
    }
}
