﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Two_practice
{
   public class Car
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public int Wheel { get; set; }

        public virtual void Run() 
        
        {
            Console.WriteLine("车都会跑");
        }
    }
}
