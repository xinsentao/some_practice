﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public abstract class Animal
    {
         public string Aname { get; set; }
        public string Acolor { get; set; }
        public virtual void move()
        {
            Console.WriteLine("行为：移动");
        }
    }
}
