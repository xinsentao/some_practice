﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Car
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            Console.WriteLine("请输入车的名称：");
            car.CarName = Console.ReadLine();
            Console.WriteLine("请输入车的颜色：");
            car.CarColor = Console.ReadLine();
            Console.WriteLine("请输入车轮数量：");
            car.Wheel = int.Parse(Console.ReadLine());

            car.Start();
        }
    }
}
