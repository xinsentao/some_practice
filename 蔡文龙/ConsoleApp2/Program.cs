﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ConsoleApp2.Garage;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
           
            {
                Car car = new Car();
                Garage garage = new Garage();

                Console.WriteLine("请输入车子的颜色");
                car.Carcolor = Console.ReadLine();
                Console.WriteLine("请输入车子的名字");
                car.Carname = Console.ReadLine();
                Console.WriteLine("请输入车子的轮子");
                car.Carwheels = int.Parse(Console.ReadLine());

                if (car.Carwheels >= 4)
                {
                    car.Run();
                }
                else
                {
                    Console.WriteLine("请输入修车厂的电话");
                    garage.Garagephone = int.Parse(Console.ReadLine());
                    Console.WriteLine("请输入修车厂的名字");
                    garage.Garagename = Console.ReadLine();
                    Console.WriteLine("请输入修车厂的地址");
                    garage.Garageaddress = Console.ReadLine();
                    garage.Garagerepaire();
                }


            }

        }
    }
    }

