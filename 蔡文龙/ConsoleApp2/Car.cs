﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
  
        public class Car : Garage
        {
            public string Carcolor { get; set; }
            public string Carname { set; get; }
            public int Carwheels { set; get; }
            public void Run()
            {

                Console.WriteLine("车子能跑");

            }
        }
    
}
