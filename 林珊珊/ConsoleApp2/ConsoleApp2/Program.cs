﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            Console.WriteLine("请输入车的名字:");
            car.CarName = Console.ReadLine();
            Console.WriteLine("请输入车的颜色:");
            car.CarColor = Console.ReadLine();
            Console.WriteLine("请输入车轮的数量:");
            car.Wheel = int.Parse(Console.ReadLine());

            car.Start();
        }
    }
}
