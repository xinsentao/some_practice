﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    public class Car:Garage

    {
        internal static readonly int wheels;

        public String Carname { get; set; }
        
        public String Carcolor { get; set; }
       
        public int   Carwheels{ get; set; }

        public void Run()
        {

            if (Carwheels >= 4)
            {
                Console.WriteLine("车可以起跑");
            }
            else
            {
                Console.WriteLine("请送去修车厂维修");
            }
        }
    }
    }

