﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();

            Console.WriteLine("请输入车的名称：");
            car.Carname = Console.ReadLine();

            Console.WriteLine("请输入车的颜色：");
            car.Carcolor = Console.ReadLine();

            Console.WriteLine("请输入车轮的数量：");
            car.Carwheels = int.Parse(Console.ReadLine());
            car.Run();
        }
    }   
}
