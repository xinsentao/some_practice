﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog dog = new Dog();

            Fish fish = new Fish();

            dog.move();

            fish.move();

            Animals a1 = new Dog();
            Animals a2 = new Fish();

            a1.move();
            a2.move();
        }
    }
}
