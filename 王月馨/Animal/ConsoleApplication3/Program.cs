﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine( "请选择你想要的宠物（猫或者狗）：");
            string str = Console.ReadLine();

            if (str.Equals ("狗"))
            {
                Console.WriteLine("请输入你给狗的名字：");
                Dog dog = new Dog();

                dog.AnimalName = Console.ReadLine();
                Console.WriteLine("请输入你要想狗的颜色：");
                dog.AnimalColor = Console.ReadLine();
                dog.Bitepeople();
            }
            if (str.Equals("猫"))
            {
                Cat cat = new Cat();

                Console.WriteLine("请输入你给狗的名字：");
                cat.AnimalName = Console.ReadLine();
                Console.WriteLine("请输入你要想猫的颜色：");
                cat.AnimalColor = Console.ReadLine();
                cat.Sleeping();
            }
            Console.WriteLine("选择结束！请去领取你的宠物");
        }
    }
}
