﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        public static string CarName { get; private set; }
        public static string CarColor { get; private set; }

        static void Main(string[] args)
        {
            Car car = new Car();
            Console.WriteLine("请输入车的名字：");
            car.CarName = Console.ReadLine();
            Console.WriteLine("请输入车的颜色：");
            car.CarColor = Console.ReadLine();
            Console.WriteLine("请输入车的轮子数：");
            car.carWheel = int.Parse(Console.ReadLine());
            car.Run();
        }
    }
}
