﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    class Fish:Animal
    {
        public void Blow()
        {
            Console.WriteLine("鱼具有的特有行为：吹泡泡");
            base.Move();
        }
    }
}
